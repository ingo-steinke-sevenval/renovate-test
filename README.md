npm install -g renovate
RENOVATE_CONFIG_FILE=./build/renovatebot renovate --token __GITLAB_ACCESS_TOKEN__ --platform gitlab --endpoint https://gitlab.com/api/v4 --log-level debug
