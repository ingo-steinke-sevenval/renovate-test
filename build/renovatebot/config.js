module.exports = {
  extends: ['config:base'],
  baseBranches: [
    'develop'
  ],
  gitAuthor: 'Renovate Bot <ingo.steinke+bot+renovateapp@sevenval.com>',
}
